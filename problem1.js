const fs = require('fs');
const path = require('path')

function writeFile(filePath, data) {
    // body...
    // console.log(`files ${filePath}`)
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            fs.writeFile(filePath, data, function (err) {
                // body...
                if(err){
                    reject(err);
                }
                else{
                    resolve(`Done`);
                }
            });
        }, 1000)
    })
}

// deleting files
function deleteFile(filePath) {
    // body...
    // console.log(`Deleting ${filePath}`);
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            fs.unlink(filePath, function(err){
                if(err){
                    reject(err)
                }
                else{
                    console.log(`Deleted ${filePath}`);
                    resolve();
                }
            });

        }, 2000);
    });

}


function generateFiles(n) {
    // body...
    let promises = [];
    for(let i = 0; i < n; i++){
        promises.push(writeFile(`./files/${i}.txt`, i.toString()));
    }
    return Promise.all(promises);
}

// generateFiles(10);


// function deleteFiles() {
//     const files = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
//     let p = Promise.resolve();
//     files.forEach(function (value){
//         p = p.then(() => {
//             return deleteFile(`./files/${value}.txt`)
//         })
//     });
//     p.then(() => {
//         console.log("Done");
//     });
// }

// generateFiles(10).then(() => {
//     deleteFiles();
// });


// by using reduce method to delete files one after another asynchronously

function deleteFiles() {
    // body...
    const files = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    return files
    .reduce((acc, cur) => {
        return acc.then(() => {
            return deleteFile(`./files/${cur}.txt`);
        });

    }, Promise.resolve()).then(() => {
        console.log('Done!');
    });

}

generateFiles(10).then(() => {
    deleteFiles();
});
