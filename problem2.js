const fs = require('fs');
const { resolve } = require('path');
const path = require('path');

const lipsum = path.resolve(__dirname, './lipsum.txt');
const lipsumToUpper = path.resolve(__dirname, './lipsumToUpper.txt');
const lipsumToLower = path.resolve(__dirname, './lipsumToLower.txt');
const lipsumSorted = path.resolve(__dirname, './lipsumSorted.txt');
const lipsumSentence = path.resolve(__dirname, './lipsumSentence.txt')
const filenames = path.resolve(__dirname, './filenames.txt');

const readFile = (filePath) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    })
}

const readFileUpper = (data) => {
    return new Promise((resolve, reject) => {
        let upper = data.toString().toUpperCase();
        resolve(upper);
    })
}

const writeFile = (filePath, content) => {
    fs.writeFile(filePath, content, (err) => {
        if (err) {
            reject(err);
        } else {
            resolve();
        }
    })

}

const readFileLower = (data) => {
    return new Promise((resolve, reject) => {
        let lower = data.toString().toLowerCase();
        resolve(lower);
    })
}

const appendFile = (filePath, data) => {
    return new Promise((resolve, reject) => {
        fs.appendFile(filePath, data, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        })

    })
}

const splitBySentence = (data) => {
    let newData = data.toString().split(/[!.\n]/);
    let sentence = [];
    newData = newData.filter(sentence => sentence);
    for (let i = 0; i < newData.length; i++) {
        if (newData[i] === ' ') {
            continue;
        }
        let str = newData[i].trim();
        sentence.push(str.charAt(0).toUpperCase() + str.substring(1) + '.');
    }
    return sentence;
}

const deleteFile = (data) => {
    let filePath = data.split('\n');
    return new Promise((resolve, reject) => {
        for (let i = 0; i < filePath.length; i++) {
            fs.unlink(filePath[i], function (err) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            })
        }
    });
}

const sorted = (data,filePath)=>{
    let files=data.split('\n');
    for(let i=0;i<files.length;i++){
        readFile(files[i])
        .then((data) => {
            let content=data.split('\n').sort();
            content.forEach(line => {
                appendFile(filePath,`${line}\n`);
            })

        })
    }
}


readFile(lipsum)
    .then((data) => readFileUpper(data))
    .then((data) => writeFile(lipsumToUpper, data))
    .then(() => writeFile(filenames, lipsumToUpper))
    .then(() => readFile(lipsumToUpper))
    .then((data) => readFileLower(data))
    .then((data) => writeFile(lipsumToLower, data))
    .then(() => appendFile(filenames, `\n${lipsumToLower}`))
    .then(() => readFile(lipsumToLower))
    .then((data) => splitBySentence(data))
    .then((data) => {
        return data.reduce((acc,currentLine) =>{
            return acc.then(()=>{
                return appendFile(lipsumSentence, `${currentLine}\n`);
            })
        },Promise.resolve())
    })
    .then(() => appendFile(filenames, `\n${lipsumSentence}`))
    .then(() => readFile(filenames))
    .then((data)=>sorted(data,sorted))
    .then(()=>appendFile(filenames,`\n${lipsumSorted}`))
    .then(()=>readFile(filenames))
    .then((data) => deleteFile(data))
    .catch((err) => console.log(err));
